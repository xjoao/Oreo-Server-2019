﻿namespace Oreo.Core.FigureData.Legacy
{
    class Figure
    {
        private readonly string Part;
        private readonly string PartId;
        public string Gender;
        private readonly string Colorable;
        public Figure(string part, string partId, string gender, string colorable)
        {
            Part = part;
            PartId = partId;
            Gender = gender;
            Colorable = colorable;
        }
    }
}