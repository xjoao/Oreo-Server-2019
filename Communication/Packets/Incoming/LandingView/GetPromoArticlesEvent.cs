﻿using System.Collections.Generic;
using Oreo.HabboHotel.LandingView.Promotions;
using Oreo.Communication.Packets.Outgoing.LandingView;

namespace Oreo.Communication.Packets.Incoming.LandingView
{
    class GetPromoArticlesEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            OreoServer.GetGame().GetLandingManager().LoadHallOfFame();
        }
    }
}
