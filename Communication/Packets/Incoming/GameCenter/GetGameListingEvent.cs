﻿
using System.Collections.Generic;

using Oreo.HabboHotel.Games;
using Oreo.Communication.Packets.Outgoing.GameCenter;

namespace Oreo.Communication.Packets.Incoming.GameCenter
{
    class GetGameListingEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            ICollection<GameData> Games = OreoServer.GetGame().GetGameDataManager().GameData;

            Session.SendMessage(new GameListComposer(Games));
        }
    }
}
