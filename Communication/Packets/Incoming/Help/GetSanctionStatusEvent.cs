﻿using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Help;

namespace Oreo.Communication.Packets.Incoming.Help
{
    class GetSanctionStatusEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new SanctionStatusComposer());
        }
    }
}
