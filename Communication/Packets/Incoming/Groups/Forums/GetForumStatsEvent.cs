﻿using Oreo.Communication.Packets.Outgoing.Groups;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.HabboHotel.GameClients;
using Oreo.HabboHotel.Groups.Forums;

namespace Oreo.Communication.Packets.Incoming.Groups
{
    class GetForumStatsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var GroupForumId = Packet.PopInt();

            GroupForum Forum;
            if (!OreoServer.GetGame().GetGroupForumManager().TryGetForum(GroupForumId, out Forum))
            {
                OreoServer.GetGame().GetClientManager().SendMessage(RoomNotificationComposer.SendBubble("forums_thread_hidden", "O fórum que você está tentando acessar não existe mais.", ""));
                return;
            }

            Session.SendMessage(new ForumDataComposer(Forum, Session));

        }
    }
}
