﻿using System.Collections.Generic;
using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Navigator;
using Oreo.HabboHotel.Navigator;

namespace Oreo.Communication.Packets.Incoming.Navigator
{
    class GetNavigatorFlatsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            ICollection<SearchResultList> Categories = OreoServer.GetGame().GetNavigator().GetEventCategories();

            Session.SendMessage(new NavigatorFlatCatsComposer(Categories, Session.GetHabbo().Rank));
        }
    }
}