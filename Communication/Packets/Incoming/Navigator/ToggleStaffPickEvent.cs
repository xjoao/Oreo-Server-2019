﻿using Oreo.HabboHotel.Navigator;
using Oreo.HabboHotel.GameClients;
using Oreo.Database.Interfaces;
using Oreo.HabboHotel.Rooms;
using Oreo.Communication.Packets.Outgoing.Navigator;
using Oreo.Communication.Packets.Outgoing.Rooms.Settings;

namespace Oreo.Communication.Packets.Incoming.Navigator
{
    class ToggleStaffPickEvent : IPacketEvent
    {
        public void Parse(GameClient session, ClientPacket packet)
        {
            GameClient TargetClient = OreoServer.GetGame().GetClientManager().GetClientByUsername(session.GetHabbo().CurrentRoom.OwnerName);
            if (!session.GetHabbo().GetPermissions().HasRight("room.staff_picks.management"))
                return;

            Room room = null;
            if (!OreoServer.GetGame().GetRoomManager().TryGetRoom(packet.PopInt(), out room))
                return;

            StaffPick staffPick = null;
            if (!OreoServer.GetGame().GetNavigator().TryGetStaffPickedRoom(room.Id, out staffPick))
            {
                if (OreoServer.GetGame().GetNavigator().TryAddStaffPickedRoom(room.Id))
                {
                    using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.SetQuery("INSERT INTO `navigator_staff_picks` (`room_id`,`image`) VALUES (@roomId, null)");
                        dbClient.AddParameter("roomId", room.Id);
                        dbClient.RunQuery();
                    }
                    OreoServer.GetGame().GetAchievementManager().ProgressAchievement(TargetClient, "ACH_Spr", 1, false);
                }
            }
            else
            {
                if (OreoServer.GetGame().GetNavigator().TryRemoveStaffPickedRoom(room.Id))
                {
                    using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.SetQuery("DELETE FROM `navigator_staff_picks` WHERE `room_id` = @roomId LIMIT 1");
                        dbClient.AddParameter("roomId", room.Id);
                        dbClient.RunQuery();
                    }
                }
            }

            room.SendMessage(new RoomSettingsSavedComposer(room.RoomId));
            room.SendMessage(new RoomInfoUpdatedComposer(room.RoomId));
        }
    }
}