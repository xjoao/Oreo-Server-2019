﻿using System.Collections.Generic;

using Oreo.HabboHotel.Users.Inventory.Bots;
using Oreo.Communication.Packets.Outgoing.Inventory.Bots;

namespace Oreo.Communication.Packets.Incoming.Inventory.Bots
{
    class GetBotInventoryEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session.GetHabbo().GetInventoryComponent() == null)
                return;

            var Bots = Session.GetHabbo().GetInventoryComponent().GetBots();
            Session.SendMessage(new BotInventoryComposer(Bots));
        }
    }
}
