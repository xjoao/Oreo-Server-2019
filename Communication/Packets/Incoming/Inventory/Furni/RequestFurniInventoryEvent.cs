﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Oreo.HabboHotel.Items;
using Oreo.Communication.Packets.Outgoing.Inventory.Furni;



namespace Oreo.Communication.Packets.Incoming.Inventory.Furni
{
    class RequestFurniInventoryEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            var FloorItems = Session.GetHabbo().GetInventoryComponent().GetFloorItems();
            var WallItems = Session.GetHabbo().GetInventoryComponent().GetWallItems();

            var allItems = new List<Item>();
            allItems.AddRange(FloorItems);
            allItems.AddRange(WallItems);

            if (allItems.Count == 0)
            {
                Session.SendMessage(new FurniListComposer());
                return;
            }

            var slots = new Dictionary<int, List<Item>>();
            var currentSlot = 0;
            foreach (var item in allItems)
            {
                if (!slots.ContainsKey(currentSlot))
                    slots.Add(currentSlot, new List<Item>());

                var items = slots[currentSlot];
                if (items.Count > 700)
                {
                    currentSlot++;
                    slots.Add(currentSlot, new List<Item>());
                    items = slots[currentSlot];
                }

                items.Add(item);


            }
            var i = 0;

            foreach (var items in slots.Values)
            {
                Session.SendMessage(new FurniListComposer(items, i, slots.Count));
                i++;
            }
        }

    }
}
