﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Oreo.Communication.Packets.Outgoing.Misc;

namespace Oreo.Communication.Packets.Incoming.Misc
{
    class PongMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Session.TimePingedReceived = DateTime.Now;
        }
    }
}
