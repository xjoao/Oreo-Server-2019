﻿using System.Linq;
using System.Collections.Generic;
using Oreo.HabboHotel.Rooms;
using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Catalog;

namespace Oreo.Communication.Packets.Incoming.Catalog
{
	class GetPromotableRoomsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            List<RoomData> Rooms = Session.GetHabbo().UsersRooms;
            Rooms = Rooms.Where(x => (x.Promotion == null || x.Promotion.TimestampExpires < OreoServer.GetUnixTimestamp())).ToList();
            Session.SendMessage(new PromotableRoomsComposer(Rooms));
        }
    }
}
