﻿using System.Collections.Generic;
using Oreo.HabboHotel.GameClients;
using Oreo.HabboHotel.Quests;
using Oreo.Communication.Packets.Incoming;

namespace Oreo.Communication.Packets.Incoming.Quests
{
    public class GetQuestListEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            OreoServer.GetGame().GetQuestManager().GetList(Session, null);
        }
    }
}