﻿using Oreo.HabboHotel.GameClients;

namespace Oreo.Communication.Packets.Incoming.Quiz
{
	class CheckQuizTypeEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            OreoServer.GetGame().GetAchievementManager().ProgressAchievement(Session, "ACH_SafetyQuizGraduate", 1, false);
        }
    }
}
