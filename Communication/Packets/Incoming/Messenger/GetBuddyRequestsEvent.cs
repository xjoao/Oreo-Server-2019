﻿using System.Linq;
using System.Collections.Generic;

using Oreo.HabboHotel.Users.Messenger;
using Oreo.Communication.Packets.Outgoing.Messenger;

namespace Oreo.Communication.Packets.Incoming.Messenger
{
    class GetBuddyRequestsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            ICollection<MessengerRequest> Requests = Session.GetHabbo().GetMessenger().GetRequests().ToList();

            Session.SendMessage(new BuddyRequestsComposer(Requests));
        }
    }
}
