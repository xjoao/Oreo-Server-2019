﻿using Oreo.Communication.Packets.Incoming;
using Oreo.HabboHotel.GameClients;

namespace Oreo.Communication.Packets
{
    public interface IPacketEvent
    {
        void Parse(GameClient Session, ClientPacket Packet);
    }
}