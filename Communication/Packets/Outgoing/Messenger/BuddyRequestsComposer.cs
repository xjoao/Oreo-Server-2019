﻿using System.Collections.Generic;
using Oreo.HabboHotel.Users.Messenger;
using Oreo.HabboHotel.Cache.Type;

namespace Oreo.Communication.Packets.Outgoing.Messenger
{
	class BuddyRequestsComposer : ServerPacket
    {
        public BuddyRequestsComposer(ICollection<MessengerRequest> Requests)
            : base(ServerPacketHeader.BuddyRequestsMessageComposer)
        {
			WriteInteger(Requests.Count);
			WriteInteger(Requests.Count);

            foreach (MessengerRequest Request in Requests)
            {
				WriteInteger(Request.From);
				WriteString(Request.Username);

                UserCache User = OreoServer.GetGame().GetCacheManager().GenerateUser(Request.From);
				WriteString(User != null ? User.Look : "");
            }
        }
    }
}
