﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Oreo.HabboHotel.Items;

namespace Oreo.Communication.Packets.Outgoing.Rooms.Engine
{
    class RoomSpectatorComposer : ServerPacket
    {
        public RoomSpectatorComposer()
            : base(ServerPacketHeader.RoomSpectatorComposer)
        {
        }
    }
}