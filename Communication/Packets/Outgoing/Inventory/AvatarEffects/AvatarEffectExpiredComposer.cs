﻿
using Oreo.HabboHotel.Users.Effects;

namespace Oreo.Communication.Packets.Outgoing.Inventory.AvatarEffects
{
	class AvatarEffectExpiredComposer : ServerPacket
    {
        public AvatarEffectExpiredComposer(AvatarEffect Effect)
            : base(ServerPacketHeader.AvatarEffectExpiredMessageComposer)
        {
			WriteInteger(Effect.SpriteId);
        }
    }
}
