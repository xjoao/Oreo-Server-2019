﻿using Oreo.Core;

namespace Oreo.Communication.RCON.Commands.Hotel
{
    class ReloadServerSettingsCommand : IRCONCommand
    {
        public string Description => "Se utiliza para recarregar as configurações";
        public string Parameters => "";

        public bool TryExecute(string[] parameters)
        {
            OreoServer.GetGame().GetSettingsManager().Init();
            return true;
        }
    }
}