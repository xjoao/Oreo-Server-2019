﻿namespace Oreo.Communication.RCON.Commands.Hotel
{
    class ReloadFilterCommand : IRCONCommand
    {
        public string Description => "Se utiliza para actualizar os filtros.";
        public string Parameters => "";

        public bool TryExecute(string[] parameters)
        {
            OreoServer.GetGame().GetChatManager().GetFilter().InitWords();
            OreoServer.GetGame().GetChatManager().GetFilter().InitCharacters();
            return true;
        }
    }
}