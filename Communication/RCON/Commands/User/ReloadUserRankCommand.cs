﻿using Oreo.Communication.Packets.Outgoing.Moderation;
using Oreo.Database.Interfaces;
using Oreo.HabboHotel.GameClients;

namespace Oreo.Communication.RCON.Commands.User
{
    class ReloadUserRankCommand : IRCONCommand
    {
        public string Description
        {
            get { return "Este comando é usado para recarregar uma classificação e permissões de usuários."; }
        }

        public string Parameters
        {
            get { return "%userId%"; }
        }

        public bool TryExecute(string[] parameters)
        {
            int userId = 0;
            if (!int.TryParse(parameters[0].ToString(), out userId))
                return false;

            GameClient client = OreoServer.GetGame().GetClientManager().GetClientByUserID(userId);
            if (client == null || client.GetHabbo() == null)
                return false;

            using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `rank` FROM `users` WHERE `id` = @userId LIMIT 1");
                dbClient.AddParameter("userId", userId);
                client.GetHabbo().Rank = dbClient.getInteger();
            }

            client.GetHabbo().GetPermissions().Init(client.GetHabbo());

            if (client.GetHabbo().GetPermissions().HasRight("mod_tickets"))
            {
                client.SendMessage(new ModeratorInitComposer(
                  OreoServer.GetGame().GetModerationManager().UserMessagePresets,
                  OreoServer.GetGame().GetModerationManager().RoomMessagePresets,
                  OreoServer.GetGame().GetModerationManager().GetTickets));
            }
            return true;
        }
    }
}