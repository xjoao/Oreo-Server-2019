﻿using System;
using Oreo.HabboHotel.GameClients;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.User
{
    class InfoCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_info"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Mostra as informações do servidor"; }
        }

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            #region
            TimeSpan Uptime = DateTime.Now - OreoServer.ServerStarted;
            int OnlineUsers = OreoServer.GetGame().GetClientManager().Count;
            int RoomCount = OreoServer.GetGame().GetRoomManager().Count;
            int userPeak = OreoServer.GetGame().GetClientManager().GetUserPeak();
            string ClientBuild = OreoServer.SWFRevision;
            #endregion

            Session.SendMessage(new RoomNotificationComposer("Informações",
                "<font color=\"#0489B1\" size=\"18\">[¶] Oreo Server</font>\n\n" +
                "<b>Agradecimientos a:</b>\n" +
                "\t- SergioT\n" +
                "\t- Sledmore\n" +
                "\t- Droppy\n" +
                "\t- Plus Developers\n" +
                "\t- Claudinho Sant0ro\n" +
                "<b>Oreo Developers:</b>\n" +
                "\t- Protegido\n" +
                "\t- Eric Hahn\n" +
                "\t- Xjoao\n" +
                "<b>Informação Atual</b>:\n" +
                "\t- Usuários Online: " + OnlineUsers + "\n" +
                "\t- Build: 01/12/2018" +"\n" +
                "\t- Versão: Stable & Private" + "\n" +
                "\t- Recorde de Usuários: " + Game.SessionUserRecord + "\n" +
                "\t- Salas Carregadas: " + RoomCount + "\n" +
                "\t- Tempo: " + Uptime.Days + " dia(s), " + Uptime.Hours + " hora(s) e " + Uptime.Minutes +
                " minuto(s).\n\n", "neoserver", ""));
        }
    }
}