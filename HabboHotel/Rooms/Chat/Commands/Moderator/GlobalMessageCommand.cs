﻿using System.Linq;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.HabboHotel.GameClients;
using Oreo.Core;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class GlobalMessageCommand : IChatCommand
    {
        public string PermissionRequired => "command_alert_user";
        public string Parameters => "[MENSAGE]";
        public string Description => "Enviar alerta 'BUBBLE' global";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
         

            if (Params.Length == 1)
            {
                Session.SendWhisper("Digite a mensagem.");
                return;
            }

            string Message = CommandManager.MergeParams(Params, 1);
            foreach (GameClient client in OreoServer.GetGame().GetClientManager().GetClients.ToList())
            {
                client.SendMessage(new RoomNotificationComposer("command_gmessage", "message", "" + Message + "!"));
            }
        }
    }
}
