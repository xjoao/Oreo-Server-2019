﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Oreo.HabboHotel.GameClients;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class MassBadgeCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_mass_badge"; }
        }

        public string Parameters
        {
            get { return "%badge%"; }
        }

        public string Description
        {
            get { return "Dê um emblema para todo o hotel."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Por favor digite o código que você dar como emblema para todos do hotel.");
                return;
            }

            foreach (GameClient Client in OreoServer.GetGame().GetClientManager().GetClients.ToList())
            {
                if (Client == null || Client.GetHabbo() == null || Client.GetHabbo().Username == Session.GetHabbo().Username)
                    continue;

                if (!Client.GetHabbo().GetBadgeComponent().HasBadge(Params[1]))
                {
                    Client.GetHabbo().GetBadgeComponent().GiveBadge(Params[1], true, Client);
                    Client.SendNotification("Você recebeu um emblema!");
                }
                else
                    Client.SendWhisper(Session.GetHabbo().Username + " tentou lhe dar um emblema, mas você já tem!");
            }

            Session.SendWhisper("Você deu com sucesso para todo o hotel o emblema: " + Params[1] + " badge!");
        }
    }
}
