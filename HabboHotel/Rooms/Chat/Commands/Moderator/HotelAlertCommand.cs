﻿using Oreo.Communication.Packets.Outgoing.Moderation;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.Core;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class HotelAlertCommand : IChatCommand
    {
        public string PermissionRequired => "command_hotel_alert";
        public string Parameters => "[MENSAGE]";
        public string Description => "Enviar alerta para hotel.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
           
            if (Params.Length == 1)
            {
                Session.SendWhisper("Por favor, introduzca un mensage para enviar.");
                return;
            }
            int OnlineUsers = OreoServer.GetGame().GetClientManager().Count;
            int RoomCount = OreoServer.GetGame().GetRoomManager().Count;
            string Message = CommandManager.MergeParams(Params, 1);
            OreoServer.GetGame().GetClientManager().SendMessage(new BroadcastMessageAlertComposer(Message + "\n\n<b>Informações do Hotel</b>:\n" +
            "Usuários Online: " + OnlineUsers + "\n" +
            "Salas: " + RoomCount + "\r\n" + "- " + Session.GetHabbo().Username));
            return;
        }
    }
}
