﻿
using Oreo.Core;
using Oreo.Database.Interfaces;
using Oreo.HabboHotel.GameClients;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
	class UnmuteCommand : IChatCommand
	{
		public string PermissionRequired
		{
			get { return "command_unmute"; }
		}

		public string Parameters
		{
			get { return "%username%"; }
		}

		public string Description
		{
			get { return "Desative um usuário atualmente silenciado."; }
		}

		public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
		{
           
            if (Params.Length == 1)
			{
				Session.SendWhisper("Digite o nome de usuário do usuário que deseja desmutar.");
				return;
			}

			GameClient TargetClient = OreoServer.GetGame().GetClientManager().GetClientByUsername(Params[1]);
			if (TargetClient == null || TargetClient.GetHabbo() == null)
			{
				Session.SendWhisper("Ocorreu um erro ao encontrar esse usuário, talvez eles não estejam online.");
				return;
			}

			using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
			{
                dbClient.runFastQuery("UPDATE `users` SET `time_muted` = '0' WHERE `id` = '" + TargetClient.GetHabbo().Id + "' LIMIT 1");
            }

            if (TargetClient.GetHabbo().TimeMuted == 0)
            {
                TargetClient.SendNotification("You have been un-muted by " + Session.GetHabbo().Username + "!");
                Session.SendWhisper("You have successfully un-muted " + TargetClient.GetHabbo().Username + "!");
            }


        }
    }
}