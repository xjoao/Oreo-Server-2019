﻿using Oreo.Core;
using Oreo.HabboHotel.GameClients;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class DisconnectCommand : IChatCommand
    {
        public string PermissionRequired => "command_disconnect";
        public string Parameters => "[USUARIO]";
        public string Description => "Desconecte um usuário.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
      

            if (Params.Length == 1)
            {
                Session.SendWhisper("Digite o nome do usuário que deseja desconectar.");
                return;
            }

            GameClient TargetClient = OreoServer.GetGame().GetClientManager().GetClientByUsername(Params[1]);
            if (TargetClient == null)
            {
                Session.SendWhisper("Opa! Provavelmente o usuário não está online.");
                return;
            }

            if (TargetClient.GetHabbo().Rank >= Session.GetHabbo().Rank)
            {
                Session.SendWhisper("Você não pode desconectar esse usuário.");
                return;
            }

            if (TargetClient.GetHabbo().GetPermissions().HasRight("mod_tool") && !Session.GetHabbo().GetPermissions().HasRight("mod_disconnect_any"))
            {
                Session.SendWhisper("Você não pode desconectar esse usuário.");
                return;
            }

            TargetClient.GetConnection().Dispose();
        }
    }
}