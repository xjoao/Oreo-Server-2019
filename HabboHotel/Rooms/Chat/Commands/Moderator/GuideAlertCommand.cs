﻿using Oreo.Communication.Packets.Outgoing.Notifications;
using Oreo.Core;
using System;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class GuideAlertCommand : IChatCommand
    {
        public string PermissionRequired => "command_guide_alert";
        public string Parameters => "[MENSAGE]";
        public string Description => "Envie uma mensagem de alerta para todos os funcionários on-line.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
          
            if (Session.GetHabbo()._guidelevel < 1)
            {
                Session.SendWhisper("Você não pode enviar alertas para guias, se não estiver rank.");
                return;
              
            }
            if (Params.Length == 1)
            {
                Session.SendWhisper("Digite a mensagem que deseja enviar.");
                return;
            }

            string Message = CommandManager.MergeParams(Params, 1);
            OreoServer.GetGame().GetClientManager().GuideAlert(new MOTDNotificationComposer("[GUÍAS][" + Session.GetHabbo().Username + "]\r\r" + Message));
            return;
        }
    }
}