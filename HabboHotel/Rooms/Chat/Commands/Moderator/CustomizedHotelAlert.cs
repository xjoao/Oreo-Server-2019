﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Oreo.Communication.Packets.Outgoing.Moderation;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.Core;

namespace Oreo.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class CustomizedHotelAlert : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_update"; }
        }

        public string Parameters
        {
            get { return "%message%"; }
        }

        public string Description
        {
            get { return "Enviar uma mensagem custom para todo o Hotel"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
       
            if (Params.Length == 1)
                {
                    Session.SendWhisper("Por favor, indique a mensagem para enviar.");
                    return;
                }

                string Message = CommandManager.MergeParams(Params, 1);
                OreoServer.GetGame().GetClientManager().SendMessage(new RoomCustomizedAlertComposer("\n" + Message + "\n\n - " + Session.GetHabbo().Username + ""));
                OreoServer.GetGame().GetClientManager().SendMessage(new MassEventComposer(Message));
                Session.SendWhisper("Custom Alerta enviado!");
                return;
        }
    }
}
