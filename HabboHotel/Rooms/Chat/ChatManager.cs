﻿using Oreo.HabboHotel.Rooms.Chat.Logs;
using Oreo.HabboHotel.Rooms.Chat.Filter;
using Oreo.HabboHotel.Rooms.Chat.Emotions;
using Oreo.HabboHotel.Rooms.Chat.Commands;
using Oreo.HabboHotel.Rooms.Chat.Pets.Commands;
using Oreo.HabboHotel.Rooms.Chat.Pets.Locale;
using log4net;
using Oreo.HabboHotel.Rooms.Chat.Styles;
using System;

namespace Oreo.HabboHotel.Rooms.Chat
{
    public sealed class ChatManager
    {
        private static readonly ILog log = LogManager.GetLogger("Oreo.HabboHotel.Rooms.Chat.ChatManager");

        /// <summary>
        /// Chat Emoticons.
        /// </summary>
        private readonly ChatEmotionsManager _emotions;

        /// <summary>
        /// Chatlog Manager
        /// </summary>
        private readonly ChatlogManager _logs;

        /// <summary>
        /// Filter Manager.
        /// </summary>
        private WordFilterManager _filter;

        /// <summary>
        /// Commands.
        /// </summary>
        private readonly CommandManager _commands;

        /// <summary>
        /// Pet Commands.
        /// </summary>
        private readonly PetCommandManager _petCommands;

        /// <summary>
        /// Pet Locale.
        /// </summary>
        private readonly PetLocale _petLocale;

        /// <summary>
        /// Chat styles.
        /// </summary>
        private ChatStyleManager _chatStyles;

        /// <summary>
        /// Initializes a new instance of the ChatManager class.
        /// </summary>
        public ChatManager()
        {
            _emotions = new ChatEmotionsManager();
            _logs = new ChatlogManager();
         
            _filter = new WordFilterManager();
            _filter.InitWords();
            _filter.InitCharacters();

            _commands = new CommandManager(":");
            _petCommands = new PetCommandManager();
            _petLocale = new PetLocale();
      
            _chatStyles = new ChatStyleManager();
            _chatStyles.Init();

            log.Info("Gerenciados de Chat - Pronto!");
        }

        public ChatEmotionsManager GetEmotions()
        {
            return _emotions;
        }

        public ChatlogManager GetLogs()
        {
            return _logs;
        }

        public WordFilterManager GetFilter()
        {
            return _filter;
        }

        public CommandManager GetCommands()
        {
            return _commands;
        }

        public PetCommandManager GetPetCommands()
        {
            return _petCommands;
        }

        public PetLocale GetPetLocale()
        {
            return _petLocale;
        }

        public ChatStyleManager GetChatStyles()
        {
            return _chatStyles;
        }
    }
}
