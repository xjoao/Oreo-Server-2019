﻿using System.Collections.Generic;
using log4net;
using Oreo.Database.Interfaces;
using System;
using System.Data;

namespace Oreo.HabboHotel.Badges
{
    public class BadgeManager
    {
        private static readonly ILog log = LogManager.GetLogger("Oreo.HabboHotel.Badges.BadgeManager");

        private readonly Dictionary<string, BadgeDefinition> _badges;

        public BadgeManager()
        {
			_badges = new Dictionary<string, BadgeDefinition>();
        }

        public void Init()
        {
            BadgeDao.LoadBadges(_badges);

            log.Info("Emblemas: " + _badges.Count + " emblemas definidos.");
        }

        public bool TryGetBadge(string BadgeCode, out BadgeDefinition Badge)
        {
            return _badges.TryGetValue(BadgeCode.ToUpper(), out Badge);
        }
    }
}