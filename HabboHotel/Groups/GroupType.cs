﻿namespace Oreo.HabboHotel.Groups
{
    public enum GroupType
    {
        OPEN,
        LOCKED,
        PRIVATE
    }
}
