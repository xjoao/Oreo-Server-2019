﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using Oreo.Database.Interfaces;
using log4net;

namespace Oreo.HabboHotel.Games
{
    public class GameDataManager
    {
        private static readonly ILog log = LogManager.GetLogger("Oreo.HabboHotel.Games.GameDataManager");

        private readonly Dictionary<int, GameData> _games;

        public GameDataManager()
        {
            _games = new Dictionary<int, GameData>();

            Init();
        }

        public void Init()
        {
            GameDataDao.LoadGameData(_games);

            log.Info("Jogos - Prontos!");
        }

        public bool TryGetGame(int GameId, out GameData GameData)
        {
            if (_games.TryGetValue(GameId, out GameData))
                return true;
            return false;
        }

        public int GetCount()
        {
            int GameCount = 0;
            foreach (GameData Game in _games.Values.ToList())
            {
                if (Game.GameEnabled)
                    GameCount += 1;
            }
            return GameCount;
        }

        public ICollection<GameData> GameData
        {
            get
            {
                return _games.Values;
            }
        }
    }
}