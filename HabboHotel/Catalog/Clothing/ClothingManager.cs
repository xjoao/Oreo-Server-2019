﻿using System;
using System.Data;
using System.Collections.Generic;
using Oreo.Database.Interfaces;

namespace Oreo.HabboHotel.Catalog.Clothing
{
    public class ClothingManager
    {
        private Dictionary<int, ClothingItem> _clothing;

        public ClothingManager()
        {
            _clothing = new Dictionary<int, ClothingItem>();
           
            Init();
        }

        public void Init()
        {
            if (_clothing.Count > 0)
                _clothing.Clear();

            using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`clothing_name`,`clothing_parts` FROM `catalog_clothing`");
                using (var reader = dbClient.ExecuteReader())
                    while (reader.Read())
                        _clothing.Add(reader.GetInt32("id"), new ClothingItem(reader.GetInt32("id"), reader.GetString("clothing_name"), reader.GetString("clothing_parts")));

            }
        }

        public bool TryGetClothing(int ItemId, out ClothingItem Clothing)
        {
            if (_clothing.TryGetValue(ItemId, out Clothing))
                return true;
            return false;
        }

        public ICollection<ClothingItem> GetClothingAllParts
        {
            get { return _clothing.Values; }
        }
    }
}
