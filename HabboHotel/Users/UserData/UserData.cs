﻿#region
using System.Collections.Generic;
using Oreo.HabboHotel.Achievements;
using Oreo.HabboHotel.Rooms;
using Oreo.HabboHotel.Users.Badges;
using Oreo.HabboHotel.Users.Messenger;
using Oreo.HabboHotel.Users.Relationships;
using System.Collections.Concurrent;
using Oreo.HabboHotel.Subscriptions;
#endregion


namespace Oreo.HabboHotel.Users.UserData
{
    public class UserData
    {
        public int userID;
        public Habbo user;

        public Dictionary<int, Relationship> Relations;
        public ConcurrentDictionary<string, UserAchievement> achievements;
        public List<Badge> badges;
        public List<int> favouritedRooms;
        public Dictionary<int, MessengerRequest> requests;
        public Dictionary<int, MessengerBuddy> friends;
        public Dictionary<int, int> quests;
        public List<RoomData> rooms;
        public Dictionary<string, Subscription> subscriptions;
        public List<string> Tags;

        public UserData(int userID, ConcurrentDictionary<string, UserAchievement> achievements, List<int> favouritedRooms, List<string> Tags,
            List<Badge> badges, Dictionary<int, MessengerBuddy> friends, Dictionary<int, MessengerRequest> requests, List<RoomData> rooms, Dictionary<int, int> quests, Habbo user,
            Dictionary<int, Relationship> Relations, Dictionary<string, Subscription> subscriptions)
        {
            this.userID = userID;
            this.achievements = achievements;
            this.favouritedRooms = favouritedRooms;
            this.Tags = Tags;
            this.badges = badges;
            this.friends = friends;
            this.requests = requests;
            this.rooms = rooms;
            this.quests = quests;
            this.user = user;
            this.Relations = Relations;
            this.subscriptions = subscriptions;
        }
    }
}
