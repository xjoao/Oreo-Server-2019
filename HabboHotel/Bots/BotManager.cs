﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using log4net;
using Oreo.Database.Interfaces;
using Oreo.HabboHotel.Rooms.AI.Responses;
using Oreo.HabboHotel.Rooms.AI;

namespace Oreo.HabboHotel.Bots
{
    public class BotManager
    {
        private static readonly ILog log = LogManager.GetLogger("Oreo.HabboHotel.Rooms.AI.BotManager");
        private List<BotResponse> _responses;

        public BotManager()
        {
			_responses = new List<BotResponse>();

			Init();
        }

        public void Init()
        {
            BotDao.LoadBotResponses(_responses);

        }


        public BotResponse GetResponse(BotAIType AiType, string Message)
        {
            foreach (BotResponse Response in _responses.Where(X => X.AiType == AiType).ToList())
            {
                if (Response.KeywordMatched(Message))
                {
                    return Response;
                }
            }

            return null;
        }
    }
}
