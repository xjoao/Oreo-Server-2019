﻿using System;

using Oreo.HabboHotel.Rooms;
using Oreo.HabboHotel.GameClients;
using Oreo.Database.Interfaces;
using System.Data;
using Oreo.Communication.Packets.Outgoing.Rooms.Notifications;
using Oreo.HabboHotel.Items.Wired;
using Oreo.Communication.Packets.Outgoing.Inventory.AvatarEffects;

namespace Oreo.HabboHotel.Items.Interactor
{
    public class InteractorWmTotem : IFurniInteractor
    {
        public void OnPlace(GameClient Session, Item Item)
        {
           
        }

        public void OnRemove(GameClient Session, Item Item)
        {
        }

        public void OnTrigger(GameClient Session, Item Item, int Request, bool HasRights)
        {
            if (!HasRights)
                return;

            var room = Session.GetHabbo().CurrentRoom;
            var squareitem = room.GetGameMap().GetRoomItemForSquare(Item.GetX, Item.GetY);

            var hasplanet = false;
            var hashead = false;
            var hasleg = false;
            var currentplanet = 0;
            var currentleg = 0;
            var currenthead = 0;
            var Modes = Item.GetBaseItem().Modes - 1;
            int CurrentMode;

            int NewMode;
            if (!int.TryParse(Item.ExtraData, out CurrentMode))
                CurrentMode = 0;

            if (CurrentMode <= 0)
                NewMode = 1;
            else if (CurrentMode >= Modes)
                NewMode = 0;
            else
                NewMode = CurrentMode + 1;

            Item.ExtraData = NewMode.ToString();
            Item.UpdateState();
                Item.GetRoom().GetWired().TriggerEvent(WiredBoxType.TriggerStateChanges, Session.GetHabbo(), Item);

            foreach (var item in squareitem)
            {
                if (item.GetBaseItem().InteractionType == InteractionType.Totem &&
                    item.GetBaseItem().BehaviourData == 1)
                {
                    currentplanet = Convert.ToInt32(item.ExtraData);
                    hasplanet = true;
                }
                else if (item.GetBaseItem().InteractionType == InteractionType.Totem &&
                         item.GetBaseItem().BehaviourData == 2)
                {
                    currenthead = Convert.ToInt32(item.ExtraData);
                    hashead = true;
                }
                else if (item.GetBaseItem().InteractionType == InteractionType.Totem &&
                         item.GetBaseItem().BehaviourData == 3)
                {
                    currentleg = Convert.ToInt32(item.ExtraData);
                    hasleg = true;
                }
            }

            if (hasleg && hasplanet && hashead)
            {
                if (OreoServer.GetUnixTimestamp() - Session.GetHabbo().LastTotem <= 3600) //if < 1h
                {
                    Session.SendWhisper("Você já pegou o efeito, tente novamente em uma hora!");
                    return;
                }
                var needinsert = false;
                if (currentplanet == 0 && (currentleg == 3 || currentleg == 7 || currentleg == 11) &&
                    (currenthead == 6 || currenthead == 10 || currenthead == 14))
                {
                    using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.RunQuery(
                            $"INSERT INTO user_effects (user_id,effect_id,quantity) VALUE ('{Session.GetHabbo().Id}', '24', '2')");
                    }
                    needinsert = true;
                }
                else if (currentplanet == 1 && (currentleg == 1 || currentleg == 5 || currentleg == 9) &&
                         (currenthead == 4 || currenthead == 8 || currenthead == 12))
                {
                    using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.RunQuery(
                            $"INSERT INTO user_effects (user_id,effect_id,quantity) VALUE ('{Session.GetHabbo().Id}', '25', '1')");
                        dbClient.RunQuery(
                            $"INSERT INTO user_effects (user_id,effect_id,quantity) VALUE ('{Session.GetHabbo().Id}', '26', '1')");
                    }
                    needinsert = true;
                }
                else if (currentplanet == 2 && (currentleg == 2 || currentleg == 6 || currentleg == 10) &&
                         (currenthead == 5 || currenthead == 9 || currenthead == 13))
                {
                    using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                    {
                        dbClient.RunQuery(
                            $"INSERT INTO user_effects (user_id,effect_id,quantity) VALUE ('{Session.GetHabbo().Id}', '23', '2')");
                    }
                    needinsert = true;
                }
                if (!needinsert) return;

                Session.GetHabbo().Effects().Init(Session.GetHabbo()); // reload the inventory
                Session.SendMessage(new AvatarEffectsComposer(Session.GetHabbo().Effects().GetAllEffects));

                Session.SendWhisper("Você ganhou um efeito!");
                Session.GetHabbo().LastTotem = OreoServer.GetIUnixTimestamp();

                using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.RunQuery("UPDATE users SET last_totem = '" + OreoServer.GetUnixTimestamp() +
                                      "' WHERE id = '" + Session.GetHabbo().Id + "'");
                }
            }
        }

        public void OnWiredTrigger(Item Item)
        {
            var currentMode = Convert.ToInt32(Item.ExtraData);
            int newMode;
            var modes = Item.GetBaseItem().Modes - 1;

            if (currentMode <= 0)
                newMode = 1;
            else if (currentMode >= modes)
                newMode = 0;
            else
                newMode = currentMode + 1;

            Item.ExtraData = newMode.ToString();
            Item.UpdateState();
        }
    }
}