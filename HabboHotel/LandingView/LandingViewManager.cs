﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using Oreo.Database.Interfaces;
using Oreo.HabboHotel.LandingView.Promotions;
using log4net;
using Oreo.Communication.Packets.Incoming.LandingView;

namespace Oreo.HabboHotel.LandingView
{
    public class LandingViewManager
    {
        private static readonly ILog log = LogManager.GetLogger("Oreo.HabboHotel.LandingView.LandingViewManager");

        internal BonusRareList BonusRareLists;

        private Dictionary<int, Promotion> _promotionItems;

        public Dictionary<uint, UserRank> ranks;
        public List<UserCompetition> usersWithRank;

        public LandingViewManager()
        {
            this._promotionItems = new Dictionary<int, Promotion>();

            this.LoadPromotions();

            this.LoadHallOfFame();
        }

        public void LoadHallOfFame()
        {

            ranks = new Dictionary<uint, UserRank>();
            usersWithRank = new List<UserCompetition>();

            usersWithRank = new List<UserCompetition>();
            usersWithRank.Clear();
            using (IQueryAdapter queryReactor = OreoServer.GetDatabaseManager().GetQueryReactor())
            {
                ranks = new Dictionary<uint, UserRank>();
                usersWithRank = new List<UserCompetition>();

                queryReactor.SetQuery("SELECT * FROM `users` WHERE `gotw_points` >= '1' AND `rank` = '1' ORDER BY `gotw_points` DESC LIMIT 16");
                DataTable gUsersTable = queryReactor.getTable();

                foreach (DataRow Row in gUsersTable.Rows)
                {
                    var staff = new UserCompetition(Row);
                    if (!usersWithRank.Contains(staff))
                        usersWithRank.Add(staff);
                }
            }
        }

        public void LoadPromotions()
        {
            if (this._promotionItems.Count > 0)
                this._promotionItems.Clear();

            using (var dbClient = OreoServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM `server_landing` ORDER BY `id` DESC");
                DataTable GetData = dbClient.getTable();

                if (GetData != null)
                {
                    foreach (DataRow Row in GetData.Rows)
                    {
                        this._promotionItems.Add(Convert.ToInt32(Row[0]), new Promotion((int)Row[0], Row[1].ToString(), Row[2].ToString(), Row[3].ToString(), Convert.ToInt32(Row[4]), Row[5].ToString(), Row[6].ToString()));
                    }
                }
            }


            log.Info("Landing View - Pronta! ");
        }



        public class BonusRareList
        {
            internal string Item;
            internal int Baseid, Score;
            internal BonusRareList(string item, int baseid, int score)
            {
                Item = item;
                Baseid = baseid;
                Score = score;
            }
        }
    }
}