﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Oreo
{
    class OreoStaticGameSettings
    {
        /// <summary>
        ///     The amount of credits a user will recieve every x minutes
        /// </summary>
        public const int UserCreditsUpdateAmount = 100;

        /// <summary>
        ///     The amount of pixels a user will recieve every x minutes
        /// </summary>
        public const int UserPixelsUpdateAmount = 3;

        /// <summary>
        /// Hotel is open for users
        /// </summary>
        public static bool HotelOpenForUsers = true;

        /// <summary>
        /// The hotel status
        /// </summary>
        public static bool IsGoingToBeClose = false;


        /// <summary>
        ///     The time a user will have to wait for Credits/Pixels update in minutes
        /// </summary>
        public const int UserCreditsUpdateTimer = 25;
    }
}
