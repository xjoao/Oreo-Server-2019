﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using Oreo.Database.Interfaces;
using Oreo.Database.Adapter;

namespace Oreo.Database
{
    public class DatabaseConnection : IDatabaseClient, IDisposable
    {
        private readonly IQueryAdapter _adapter;
        private readonly MySqlConnection _con;
        public DatabaseConnection(string ConnectionStr)
        {
            _con = new MySqlConnection(ConnectionStr);
            _adapter = new NormalQueryReactor(this);
        }
        public void connect()
        {
            if (_con.State == ConnectionState.Closed)
            {
                try
                {
                    _con.Open();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }
        public void disconnect()
        {
            if (_con.State == ConnectionState.Open)
            {
                _con.Close();
            }
        }
        public IQueryAdapter GetQueryReactor()
        {
            return _adapter;
        }
        public void prepare()
        {
            // nothing here
        }
        public void reportDone()
        {
            Dispose();
        }
        public MySqlCommand createNewCommand()
        {
            return _con.CreateCommand();
        }
        public void Dispose()
        {
            if (_con.State == ConnectionState.Open)
            {
                _con.Close();
            }
            _con.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}